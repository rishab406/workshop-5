import math
str="How much wood would a woodchuck chuck if a woodchuck could chuck wood"
l=str.upper().split(" ")
d={}
count = 1

for i in l:
    if i in d.keys():
        count+=1
        d.update({i:count})

    else:
        d.update({i:1})
    count=1

for i,j in sorted(d.items(), key=lambda i:(i[1],i[0]), reverse=True):
    print('{}:{}'.format(i,j))

# for word in sorted(table.keys()):
# 	print(word, end = ": ")
# 	for lineNum in sorted(table[word]):
# 		print(lineNum, end = " ")
# 	print()

