import math
str="How much wood would a woodchuck chuck if a woodchuck could chuck wood"
l=str.upper().split(" ")
d={}
count = 1
sum=0

for i in l:
    if i in d.keys():
        sum=1
        count+=1
        d.update({i:count})

    else:
        sum+=1
        d.update({i:1})

    count=1

for i,j in sorted(d.items(), key=lambda i:(i[1],i[0]), reverse=True):
    average=math.floor((100*j)/sum)
    if (average%5==0):
        star= "*" *(average//5)
    else:
        star = "*" * ((average // 5)+1)

    answer='[{}] {}%'.format(star,average)

    print('{}:{}'.format(i,answer))